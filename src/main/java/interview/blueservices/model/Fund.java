package interview.blueservices.model;

import interview.blueservices.model.enums.FundType;

public class Fund {

    public Fund(long id, String name, FundType fundType) {
        this.id = id;
        this.name = name;
        this.fundType = fundType;
    }

    private long id;
    private String name;
    private FundType fundType;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public FundType getFundType() {
        return fundType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fund fund = (Fund) o;

        if (id != fund.id) return false;
        if (name != null ? !name.equals(fund.name) : fund.name != null) return false;
        return fundType == fund.fundType;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (fundType != null ? fundType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Fund{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", fundType=" + fundType +
                '}';
    }
}
