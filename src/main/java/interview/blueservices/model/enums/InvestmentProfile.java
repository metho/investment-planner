package interview.blueservices.model.enums;

import java.math.BigDecimal;

public enum InvestmentProfile {

    SAFE(new BigDecimal("0.2"), new BigDecimal("0.75"), new BigDecimal("0.05")),
    BALANCED(new BigDecimal("0.3"), new BigDecimal("0.6"), new BigDecimal("0.1")),
    AGGRESIVE(new BigDecimal("0.4"), new BigDecimal("0.2"), new BigDecimal("0.4"));

    private final BigDecimal percentagePolish;
    private final BigDecimal percentageForeign;
    private final BigDecimal percentageMoney;

    InvestmentProfile(BigDecimal percentagePolish, BigDecimal percentageForeign, BigDecimal percentageMoney) {
        this.percentagePolish = percentagePolish;
        this.percentageForeign = percentageForeign;
        this.percentageMoney = percentageMoney;
    }

    public BigDecimal getPercentagePolish() {
        return percentagePolish;
    }

    public BigDecimal getPercentageForeign() {
        return percentageForeign;
    }

    public BigDecimal getPercentageMoney() {
        return percentageMoney;
    }
}
