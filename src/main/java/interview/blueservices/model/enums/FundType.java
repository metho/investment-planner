package interview.blueservices.model.enums;

public enum FundType {

    POLISH,
    FOREIGN,
    MONEY,
    NONE
}
