package interview.blueservices.model;

import java.math.BigDecimal;

public class InvestmentCalculation {

    public InvestmentCalculation(Fund fund, long amount, BigDecimal percentage) {
        this.fund = fund;
        this.amount = amount;
        this.percentage = percentage;
    }

    private Fund fund;
    private long amount;
    private BigDecimal percentage;

    public Fund getFund() {
        return fund;
    }

    public void setFund(Fund fund) {
        this.fund = fund;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}
