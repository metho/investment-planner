package interview.blueservices.services;

import interview.blueservices.model.enums.FundType;
import interview.blueservices.model.Fund;
import interview.blueservices.model.InvestmentCalculation;
import interview.blueservices.model.enums.InvestmentProfile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InvestmentService {

    public List<InvestmentCalculation> calculateInvestments(List<Fund> funds, InvestmentProfile investmentProfile, long amountToInvest) {

        if (funds.isEmpty() || amountToInvest <= 0) {
            return new ArrayList<>();
        }

        long amountForPolishFunds = calculateAmountForFundType(investmentProfile.getPercentagePolish(), amountToInvest);
        long amountForForeignFunds = calculateAmountForFundType(investmentProfile.getPercentageForeign(), amountToInvest);
        long amountForMoneyFunds = calculateAmountForFundType(investmentProfile.getPercentageMoney(), amountToInvest);
        long adjustedAmountToInvest = amountForPolishFunds + amountForForeignFunds + amountForMoneyFunds;
        long amountNotInvested = amountToInvest - adjustedAmountToInvest;

        List<InvestmentCalculation> investmentCalculations = splitInvestments(amountForPolishFunds, amountForForeignFunds, amountForMoneyFunds, funds, adjustedAmountToInvest);

        if (amountNotInvested > 0) {
            addUnassignedAmountAsLastResult(investmentCalculations, amountNotInvested);
        }

        return investmentCalculations;

    }

    private List<InvestmentCalculation> splitInvestments(long amountForPolishFunds, long amountForForeignFunds, long amountForMoneyFunds, List<Fund> funds, long adjustedAmountToInvest) {
        Map<FundType, List<Fund>> fundsByType = funds.stream().collect(Collectors.groupingBy(Fund::getFundType));
        List<InvestmentCalculation> investmentCalculations = new ArrayList<>();

        for (FundType fundType : fundsByType.keySet()) {
            if (FundType.POLISH.equals(fundType)) {
                investmentCalculations.addAll(splitAmountToFunds(fundsByType.get(fundType), amountForPolishFunds, adjustedAmountToInvest));
            } else if (FundType.FOREIGN.equals(fundType)) {
                investmentCalculations.addAll(splitAmountToFunds(fundsByType.get(fundType), amountForForeignFunds, adjustedAmountToInvest));
            } else if (FundType.MONEY.equals(fundType)) {
                investmentCalculations.addAll(splitAmountToFunds(fundsByType.get(fundType), amountForMoneyFunds, adjustedAmountToInvest));
            }
        }
        return investmentCalculations;
    }

    private List<InvestmentCalculation> splitAmountToFunds(List<Fund> funds, long amountForFundType, long totalAmount) {
        if (funds.isEmpty()) {
            return new ArrayList<>();
        }

        long remainder = amountForFundType % funds.size();
        amountForFundType = amountForFundType - remainder;
        long singleInvestment = amountForFundType / funds.size();

        List<InvestmentCalculation> investmentCalculations = calculateForFunds(funds, totalAmount, singleInvestment);

        if (remainder != 0) {
            adjustForReminder(investmentCalculations.get(0), remainder, totalAmount);
        }

        return investmentCalculations;
    }

    private List<InvestmentCalculation> calculateForFunds(List<Fund> funds, long totalAmount, long singleInvestment) {
        return funds.stream()
                .map(fund -> {
                    BigDecimal percentage = new BigDecimal((double) singleInvestment / totalAmount);
                    return new InvestmentCalculation(fund, singleInvestment, roundPercentage(percentage));
                })
                .collect(Collectors.toList());
    }

    private void adjustForReminder(InvestmentCalculation investmentCalculation, long remainder, long totalAmount) {
        investmentCalculation.setAmount(investmentCalculation.getAmount() + remainder);
        BigDecimal percentage = new BigDecimal((double) (investmentCalculation.getAmount()) / totalAmount);
        investmentCalculation.setPercentage(roundPercentage(percentage));
    }

    private BigDecimal roundPercentage(BigDecimal percentage) {
        return percentage.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    private long calculateAmountForFundType(BigDecimal percentage, long amountToInvest) {
        return percentage.multiply(BigDecimal.valueOf(amountToInvest)).setScale(0, RoundingMode.DOWN).longValue();
    }

    private void addUnassignedAmountAsLastResult(List<InvestmentCalculation> investmentCalculations, long amountNotInvested) {
        investmentCalculations.add(new InvestmentCalculation(new Fund(investmentCalculations.size() + 1, "unassigned", FundType.NONE), amountNotInvested, BigDecimal.ZERO));
    }
}
