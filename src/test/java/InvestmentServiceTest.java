import interview.blueservices.model.Fund;
import interview.blueservices.model.InvestmentCalculation;
import interview.blueservices.model.enums.FundType;
import interview.blueservices.model.enums.InvestmentProfile;
import interview.blueservices.services.InvestmentService;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;

public class InvestmentServiceTest {

    private final InvestmentService investmentService = new InvestmentService();

    @Test
    public void testNoFundsProvided() {
        List<InvestmentCalculation> investmentCalculations = investmentService.calculateInvestments(new ArrayList<>(), InvestmentProfile.BALANCED, 10000);
        assertTrue(investmentCalculations.isEmpty());
    }

    @Test
    public void testZeroAmountProvided() {
        List<Fund> funds = createFunds1();
        List<InvestmentCalculation> investmentCalculations = investmentService.calculateInvestments(funds, InvestmentProfile.BALANCED, 0);
        assertTrue(investmentCalculations.isEmpty());
    }

    @Test
    public void testCalculationSafeProfileNoRemainder() {
        List<Fund> funds = createFunds2();
        List<InvestmentCalculation> investmentCalculations = investmentService.calculateInvestments(funds, InvestmentProfile.SAFE, 10000);
        Collections.sort(investmentCalculations, (o1, o2) -> Long.compare(o1.getFund().getId(), o2.getFund().getId()));

        assertTrue(investmentCalculations.size() == 6);
        assertEquals(668, investmentCalculations.get(0).getAmount());
        assertEquals(new BigDecimal("0.0668"), investmentCalculations.get(0).getPercentage());
        assertEquals(666, investmentCalculations.get(1).getAmount());
        assertEquals(new BigDecimal("0.0666"), investmentCalculations.get(1).getPercentage());
        assertEquals(666, investmentCalculations.get(2).getAmount());
        assertEquals(new BigDecimal("0.0666"),investmentCalculations.get(2).getPercentage());
        assertEquals(3750, investmentCalculations.get(3).getAmount());
        assertEquals(new BigDecimal("0.3750"), investmentCalculations.get(3).getPercentage());
        assertEquals(3750, investmentCalculations.get(4).getAmount());
        assertEquals(new BigDecimal("0.3750"), investmentCalculations.get(4).getPercentage());
        assertEquals(500, investmentCalculations.get(5).getAmount());
        assertEquals(new BigDecimal("0.0500"), investmentCalculations.get(5).getPercentage());


    }

    @Test
    public void testCalculationSafeProfileWithRemainder() {
        List<Fund> funds = createFunds1();
        List<InvestmentCalculation> investmentCalculations = investmentService.calculateInvestments(funds, InvestmentProfile.SAFE, 10001);
        Collections.sort(investmentCalculations, (o1, o2) -> Long.compare(o1.getFund().getId(), o2.getFund().getId()));

        assertTrue(investmentCalculations.size() == 7);
        assertEquals(1000, investmentCalculations.get(0).getAmount());
        assertEquals(new BigDecimal("0.1000"), investmentCalculations.get(0).getPercentage());
        assertEquals(1000, investmentCalculations.get(1).getAmount());
        assertEquals(new BigDecimal("0.1000"), investmentCalculations.get(1).getPercentage());
        assertEquals(2500, investmentCalculations.get(2).getAmount());
        assertEquals(new BigDecimal("0.2500"), investmentCalculations.get(2).getPercentage());
        assertEquals(2500, investmentCalculations.get(3).getAmount());
        assertEquals(new BigDecimal("0.2500"), investmentCalculations.get(3).getPercentage());
        assertEquals(2500, investmentCalculations.get(4).getAmount());
        assertEquals(new BigDecimal("0.2500"), investmentCalculations.get(4).getPercentage());
        assertEquals(500, investmentCalculations.get(5).getAmount());
        assertEquals(new BigDecimal("0.0500"), investmentCalculations.get(5).getPercentage());
        assertEquals(1, investmentCalculations.get(6).getAmount());
        assertEquals(FundType.NONE, investmentCalculations.get(6).getFund().getFundType());
        assertEquals(BigDecimal.ZERO, investmentCalculations.get(6).getPercentage());
    }

    @Test
    public void testCalculationBalancedProfile() {
        List<Fund> funds = createFunds1();
        List<InvestmentCalculation> investmentCalculations = investmentService.calculateInvestments(funds, InvestmentProfile.BALANCED, 10000);
        Collections.sort(investmentCalculations, (o1, o2) -> Long.compare(o1.getFund().getId(), o2.getFund().getId()));

        assertTrue(investmentCalculations.size() == 6);
        assertEquals(1500, investmentCalculations.get(0).getAmount());
        assertEquals(new BigDecimal("0.1500"), investmentCalculations.get(0).getPercentage());
        assertEquals(1500, investmentCalculations.get(1).getAmount());
        assertEquals(new BigDecimal("0.1500"), investmentCalculations.get(1).getPercentage());
        assertEquals(2000, investmentCalculations.get(2).getAmount());
        assertEquals(new BigDecimal("0.2000"), investmentCalculations.get(2).getPercentage());
        assertEquals(2000, investmentCalculations.get(3).getAmount());
        assertEquals(new BigDecimal("0.2000"), investmentCalculations.get(3).getPercentage());
        assertEquals(2000, investmentCalculations.get(4).getAmount());
        assertEquals(new BigDecimal("0.2000"), investmentCalculations.get(4).getPercentage());
        assertEquals(1000, investmentCalculations.get(5).getAmount());
        assertEquals(new BigDecimal("0.1000"), investmentCalculations.get(5).getPercentage());
    }

    private List<Fund> createFunds1() {
        List<Fund> funds = new ArrayList<>();
        funds.add(new Fund(1, "Fundusz Polski 1", FundType.POLISH));
        funds.add(new Fund(2, "Fundusz Polski 2", FundType.POLISH));
        funds.add(new Fund(3, "Fundusz Zagraniczny 1", FundType.FOREIGN));
        funds.add(new Fund(4, "Fundusz Zagraniczny 2", FundType.FOREIGN));
        funds.add(new Fund(5, "Fundusz Zagraniczny 3", FundType.FOREIGN));
        funds.add(new Fund(6, "Fundusz Pieniężny 1", FundType.MONEY));
        return funds;
    }

    private List<Fund> createFunds2() {
        List<Fund> funds = new ArrayList<>();
        funds.add(new Fund(1, "Fundusz Polski 1", FundType.POLISH));
        funds.add(new Fund(2, "Fundusz Polski 2", FundType.POLISH));
        funds.add(new Fund(3, "Fundusz Polski 3", FundType.POLISH));
        funds.add(new Fund(4, "Fundusz Zagraniczny 1", FundType.FOREIGN));
        funds.add(new Fund(5, "Fundusz Zagraniczny 2", FundType.FOREIGN));
        funds.add(new Fund(6, "Fundusz Pieniężny 1", FundType.MONEY));
        return funds;
    }
}
